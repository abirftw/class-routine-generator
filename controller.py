
from copy import deepcopy
import time

from models import ClassDay, ClassWeek, Course, Teacher, hasEnoughSlotForRestOfWeek
from utils import *
import json
import pylightxl as xl
from typing import List, Tuple
YEARS = 1
LAB_SECTIONS = 2
SEMESTER = 2
MINIMUM_CLASS_SLOT = 3


def loadClassSchedule(json_file_path: str):
    class_week = ClassWeek()
    with open(json_file_path) as class_time_file:
        class_time = json.load(class_time_file)
        for day in WeekDays:
            available_times = processTimeString(
                class_time[str(day.name).capitalize()])
            for time in available_times:
                class_week.class_days[day.value].setAvailableSlot(
                    getSlotInterval(time[0], time[1]))
    return class_week


def loadCourseInfo(json_file_path: str):
    semseter_courses = []
    for _ in range(YEARS):
        semseter_courses.append([])
    with open(json_file_path) as curriculum_file:
        courses = json.load(curriculum_file)
        for course in courses:
            course_info = getCourseInfo(course['Course Code'])
            if course_info[0] <= YEARS and course_info[1] == SEMESTER:
                if course['Lab']:
                    # create course sections for lab
                    code = course['Course Code']
                    for i in range(LAB_SECTIONS):
                        course['Course Code'] = f"{code} Section {i + 1}"
                        course['Section'] = i + 1
                        semseter_courses[course_info[0] -
                                         1].append(Course(kwargs=course))
                else:
                    semseter_courses[course_info[0] -
                                     1].append(Course(kwargs=course))
    return semseter_courses


class LoadTeacherInfo():
    def __init__(self, **kwargs) -> None:
        self.xlsx_file_path = kwargs.pop('xlsx_file_path', 'input.xlsx')
        self.teacher_sheet = kwargs.pop('teacher_sheet', 'Sheet1')
        self.course_sheet = kwargs.pop('course_sheet', 'Sheet2')
        self.schedule_sheet = kwargs.pop('schedules_sheet', 'Sheet3')
        self.db = None

    def loadDB(self):
        self.db = xl.readxl(self.xlsx_file_path)

    def loadteacher(self):
        if not self.db:
            self.loadDB()
        keys = self.db.ws(self.teacher_sheet).row(row=1)
        header = True
        info = {}
        teachers = {}
        for row in self.db.ws('Sheet1').rows:
            if header:
                header = False
            else:
                for i, col in enumerate(row):
                    info[keys[i]] = col
                teachers[info['Teacher Initial']] = Teacher(kwargs=info)
        return teachers

    def loadcourse(self, semester_courses: list, teachers):
        header = True
        for row in self.db.ws(self.course_sheet).rows:
            if header:
                header = False
            else:
                teacher = teachers[row[0]]
                for i in range(1, len(row)):
                    if row[i]:
                        course_info = getCourseInfo(row[i])
                        if course_info[0] <= YEARS and course_info[1] == SEMESTER:
                            for course in semester_courses[course_info[0] - 1]:
                                if course.course_code == row[i]:
                                    course.assigned_to.append(
                                        teacher)

                    else:
                        break
        for i in range(YEARS):
            for course in semester_courses[i]:
                if len(course.assigned_to) == 0:
                    raise ValueError(
                        f"{course} doesn't have a teacher assigned")

    def loadschedule(self, teachers):
        header = True
        keys = self.db.ws(self.schedule_sheet).row(row=1)
        for row in self.db.ws(self.schedule_sheet).rows:
            if header:
                header = False
            else:
                teacher = teachers[row[0]]
                for i, col in enumerate(row):
                    if col:
                        try:
                            index = WeekDays[keys[i].upper()].value
                            available_times = processTimeString(
                                col, separator_time=';')
                            for time in available_times:
                                teacher.working_days[index].setAvailableSlot(
                                    getSlotInterval(time[0], time[1]))
                        except KeyError:
                            pass


week = loadClassSchedule('class_time.json')
class_week = [deepcopy(week) for _ in range(YEARS)]
semester_courses = loadCourseInfo('course_curriculum.json')
loadinfo = LoadTeacherInfo()
teachers = loadinfo.loadteacher()
loadinfo.loadcourse(semester_courses, teachers)
loadinfo.loadschedule(teachers)
li = [0]
dli = []
pli = [0]
xli = [0]


def printRoutine(routine_list: List[ClassWeek]):
    dash = '='*100
    print(dash)
    for routine_id, routine in enumerate(routine_list):
        print(dash)
        print(f"Each slot is {TIME_UNIT_M} minutes long")
        print(
            f"Class start time: {CLASS_START_TIME.time()} and Class end time: {CLASS_END_TIME.time()}")
        print(
            f"{ClassDay.UNAVAILABLE} represents booked slots for other reasons, {ClassDay.AVAILABLE} represents empty slots")
        print("Slots with comma separated courses can occur simultaneously")
        print("Year: ", routine_id+1)
        for day_id in range(len(WeekDays.__members__)):
            print(WeekDays(day_id).name,
                  routine.class_days[day_id].slot_map)
    print(dash)


def setFilledSlots(day_no: int, class_day: ClassDay, course: Course, to_fill: tuple, extra: bool = False):
    if extra:
        class_day.setSlotExtra(to_fill, course.course_code)
    else:
        class_day.setSlot(to_fill, course.course_code)
    course.setTaken(day_no)
    for teacher in course.assigned_to:
        teacher.working_days[day_no].setSlot(
            to_fill, course.course_code)


def resetFilledSlots(day_no: int, class_day: ClassDay, course: Course, to_reset: tuple, extra: bool = False):
    if extra:
        class_day.resetSlotLastExtra(to_reset)
    else:
        class_day.resetSlot(to_reset)
    course.resetTaken(day_no)
    for teacher in course.assigned_to:
        teacher.working_days[day_no].resetSlot(to_reset)


def isSlotAssignmentPossible(class_day: ClassDay, course: Course, to_fill: tuple) -> Tuple[bool]:
    slot_fill_possible = False
    slot_override_possilble = False
    if class_day.isSlotAvailable(to_fill):
        slot_fill_possible = True
    elif course.is_lab and class_day.isSlotLab(
            to_fill, course.course_code):
        slot_override_possilble = True
    return (slot_fill_possible, slot_override_possilble)


def generateClassRoutine(day_no: int, year_no: int, course_no: int):
    if year_no == YEARS:
        return
    if course_no == len(semester_courses[year_no]):
        return
    pli[0] += 1
    if day_no == WeekDays.THURSDAY.value + 1:
        printRoutine(class_week)
        li[0] += 1
        return
    class_day = class_week[year_no].class_days[day_no]
    semester_courses[year_no] = sorted(
        semester_courses[year_no], key=lambda course: course.weekly_quota, reverse=True)  # first order by courses that have classes left
    # then order by maximum classtime
    semester_courses[year_no] = sorted(
        semester_courses[year_no], key=lambda course: course.class_time_slots, reverse=True)
    for course_id in range(len(semester_courses[year_no])):
        course = semester_courses[year_no][course_id]
        if not course.isTaken(day_no) and course.weekly_quota > 0:
            course_slot_no = course.class_time_slots
            slot_count = 0
            for slot_id in range(len(class_day.slot_map)):
                common_slots = 0
                for teacher in course.assigned_to:
                    if teacher.working_days[day_no].slot_map[slot_id] == ClassDay.AVAILABLE:
                        common_slots += 1
                    else:
                        break
                if common_slots != len(course.assigned_to):
                    slot_count = 0
                else:
                    slot_count += 1
                    if slot_count == course_slot_no:
                        to_fill_slot = (slot_id-course_slot_no+1, slot_id+1)
                        fill, override = isSlotAssignmentPossible(class_day, course, to_fill_slot)
                        if fill or override:
                            setFilledSlots(day_no, class_day, course, to_fill_slot, override)
                            # add another course to this solution
                            generateClassRoutine(
                                day_no, year_no, course_no + 1)
                            # reset to check for another position
                            resetFilledSlots(day_no, class_day, course, to_fill_slot, override)
                            # reset slot to only the minimum slot a class can have
                            slot_count = min(0, slot_count-MINIMUM_CLASS_SLOT)
                        else:
                            slot_count = 0
    if class_day.slots_remaining == class_day.default_slot:
        # a class day without any classes is an invalid solution
        return

    for course in semester_courses[year_no]:
        # can't fill the remaining solutions, will result in an invalid solution
        if course.weekly_quota > len(WeekDays.__members__) - (day_no + 1):
            return
        elif course.weekly_quota > 0:
            # next days without necessary available teacher slots will result in an invalid solution
            if not hasEnoughSlotForRestOfWeek(day_no, course):
                return
    generateClassRoutine(day_no, year_no+1, 0)  # for same day, next year
    generateClassRoutine(day_no+1, 0, 0)  # for next day, for all


x = time.perf_counter()
generateClassRoutine(0, 0, 0)
y = time.perf_counter()
print(y - x)
print(li)  # number of solutions
print(pli[0])  # number of recursive steps
