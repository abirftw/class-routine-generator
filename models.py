from datetime import timedelta
from utils import *
LAB_SECTIONS = 2


class Teacher():
    def __init__(self, kwargs) -> None:
        self.name_initial = kwargs['Teacher Initial']
        self.name = kwargs['Full Name']
        self.designation = kwargs.get('Designation', '')
        self.department = kwargs.get('Department', '')
        self.university = kwargs.get('University', '')
        self.assigned_courses = []
        self.working_days = [ClassDay()
                             for _ in range(len(WeekDays.__members__))]

    def setAssignedCourses(self, course_list: list):
        self.assigned_courses = course_list

    def __str__(self) -> str:
        return self.name


class ClassDay():
    AVAILABLE = "Y"
    UNAVAILABLE = "X"
    SLOT_START = 0
    SLOT_END = 1

    def __init__(self, **kwargs) -> None:
        # by default all slots are blocked
        available = kwargs.pop('a', False)
        if available:
            self.slot_map = [self.AVAILABLE]*getDefaultSlotCount()
        else:
            self.slot_map = [self.UNAVAILABLE]*getDefaultSlotCount()
        self.available_slots = []
        self.slots_remaining = 0
        self.default_slot = 0

    def _updateAvailableSlots(self, slot_id: int, slot_filled: tuple):
        for_slot = self.available_slots[slot_id]
        if for_slot[self.SLOT_START] < slot_filled[self.SLOT_START] and for_slot[self.SLOT_END] > slot_filled[self.SLOT_END]:
            self.available_slots[slot_id] = (
                for_slot[self.SLOT_START], slot_filled[self.SLOT_START])
            self.available_slots.append(
                (slot_filled[self.SLOT_END], for_slot[self.SLOT_END]))
        elif for_slot[self.SLOT_START] < slot_filled[self.SLOT_START]:
            self.available_slots[slot_id] = (
                for_slot[self.SLOT_START], slot_filled[self.SLOT_START])
        elif for_slot[self.SLOT_END] > slot_filled[self.SLOT_END]:
            self.available_slots[slot_id] = (
                slot_filled[self.SLOT_END], for_slot[self.SLOT_END])
        else:
            self.available_slots.pop(slot_id)

    def setSlotExtra(self, slot: tuple, val: str):
        self.fillSlot(slot, val, join=True)

    def resetSlotLastExtra(self, slot: tuple):
        for i in range(getSlotCount(slot)):
            values = self.slot_map[slot[self.SLOT_START] + i].split(',')
            if len(values) > 1:
                self.slot_map[slot[self.SLOT_START] +
                              i] = ','.join([values[i] for i in range(len(values) - 1)])

    def setSlot(self, slot: tuple, val: str):
        self.fillSlot(slot, val)
        self.slots_remaining -= getSlotCount(slot)

    def fillSlot(self, slot: tuple, val: str, join=False):
        for i in range(getSlotCount(slot)):
            if join:
                self.slot_map[slot[self.SLOT_START] +
                              i] = ','.join([self.slot_map[slot[self.SLOT_START] + i], val])
            else:
                self.slot_map[slot[self.SLOT_START] + i] = val

    def resetSlot(self, slot: tuple):
        self.fillSlot(slot, self.AVAILABLE)
        self.slots_remaining += getSlotCount(slot)

    def resetDay(self):
        self.slot_map = [self.UNAVAILABLE]*getDefaultSlotCount()
        self.slots_remaining = 0
        for slot in self.available_slots:
            self.setAvailableSlot(slot, True)

    def setAvailableSlot(self, slot: tuple, fake=False):
        self.fillSlot(slot, self.AVAILABLE)
        slot_count = getSlotCount(slot)
        self.slots_remaining += slot_count
        if not fake:
            self.available_slots.append(slot)
            self.default_slot += slot_count

    def getAvailableSlot(self, slot_count: int, for_slot: tuple = None):
        for id, slot in enumerate(self.available_slots):
            if getSlotCount(slot) >= slot_count:
                if for_slot:
                    if slot[self.SLOT_START] <= for_slot[self.SLOT_START] and for_slot[self.SLOT_END] <= slot[self.SLOT_END]:
                        return id
                else:
                    return id
        return None

    def isSlotAvailable(self, slot: tuple):
        for i in range(getSlotCount(slot)):
            if self.slot_map[slot[self.SLOT_START] + i] != self.AVAILABLE:
                return False
        return True

    def isSlotLab(self, slot: tuple, course_code: str = None):
        for i in range(getSlotCount(slot)):
            if self.slot_map[slot[self.SLOT_START] + i].find('Section') == -1:
                return False
            elif course_code:
                section = course_code[course_code.find('Section'):]
                values = self.slot_map[slot[self.SLOT_START] + i].split(',')
                if len(values) >= LAB_SECTIONS:
                    return False
                else:
                    for value in values:
                        if value.find(section) != -1:
                            return False

        return True


class ClassWeek():
    def __init__(self) -> None:
        self.class_days = [ClassDay()
                           for _ in range(len(WeekDays.__members__))]


class Course():
    class_time_m = {
        'theory': {
            '3': 90,
            '2': 60
        },
        'lab': 180
    }
    class_weekly_quota = {
        2: 2,
        3: 2,
        0.75: 1,
        1.5: 1
    }

    def __init__(self, kwargs) -> None:
        self.course_code = kwargs['Course Code']
        self.is_lab = False
        self.course_credit = kwargs['Theory']
        if not self.course_credit:
            self.is_lab = True
            self.course_credit = kwargs['Lab']
        self.class_time_slots = self.getClassTimeSlots()
        self.weekly_quota = self.getWeeklyQuota()
        self.is_taken_for_day = [ClassDay.AVAILABLE]*len(WeekDays.__members__)
        if self.is_lab:
            self.section = kwargs.get('Section', 0)
        self.assigned_to = []

    def getClassTimeSlots(self):
        endtime = CLASS_START_TIME + timedelta(minutes=90)
        if self.is_lab:
            endtime = CLASS_START_TIME + timedelta(minutes=180)
        return getSlotCount(getSlotInterval(CLASS_START_TIME, endtime))

    def setSection(self, section: int):
        if not self.is_lab:
            raise ValueError("Theory courses can't have sections")
        self.section = section

    def setTaken(self, day_no: int):
        if self.weekly_quota - 1 < 0:
            raise ValueError(
                f"{self.course_code} has already achieved its quota")
        self.is_taken_for_day[day_no] = ClassDay.UNAVAILABLE
        self.weekly_quota -= 1

    def isTaken(self, day_no: int):
        return self.is_taken_for_day[day_no] == ClassDay.UNAVAILABLE

    def resetTaken(self, day_no: int):
        if self.is_taken_for_day[day_no] == ClassDay.AVAILABLE:
            raise ValueError(
                f"{self.course_code} doesn't have any class for {day_no}")
        self.is_taken_for_day[day_no] = ClassDay.AVAILABLE
        self.weekly_quota += 1

    def getWeeklyQuota(self):
        return self.class_weekly_quota[self.course_credit]

    def __str__(self) -> str:
        return self.course_code


def hasEnoughSlotForRestOfWeek(from_day: int, course: Course):
    course_slots_required = course.weekly_quota*course.class_time_slots
    slots_available = False
    for day_id in range(from_day + 1, len(WeekDays.__members__)):
        if not slots_available:
            slots_found = 0
            for slot_id in range(len(course.assigned_to[0].working_days[0].slot_map)):
                common_slots = 0
                for teacher in course.assigned_to:
                    if teacher.working_days[day_id].slot_map[slot_id] == ClassDay.AVAILABLE:
                        common_slots += 1
                    else:
                        break
                if common_slots != len(course.assigned_to):
                    slots_found = 0
                else:
                    slots_found += 1
                    if slots_found == course.class_time_slots:
                        course_slots_required -= slots_found
                        slots_found = 0
                        if course_slots_required == 0:
                            slots_available = True
                            break
        else:
            break
    return slots_available
