from enum import Enum
from datetime import datetime
import re
DEFAULT_TIME_FORMAT = "%I:%M%p"
CLASS_START_TIME = datetime.strptime("08:30am", DEFAULT_TIME_FORMAT)
CLASS_END_TIME = datetime.strptime("05:00pm", DEFAULT_TIME_FORMAT)
Test_START_TIME = datetime.strptime("08:30am", DEFAULT_TIME_FORMAT)
Test_END_TIME = datetime.strptime("5:00pm", DEFAULT_TIME_FORMAT)
TIME_UNIT_M = 30
TIME_UNIT_S = TIME_UNIT_M*60
course_code_pattern = re.compile(r"\w+.(\d)(\d)\d{2}(\s\w+.\d)?")


def getCourseInfo(course_id: str):
    match = course_code_pattern.search(course_id)
    if match:
        return (int(match.group(1)), int(match.group(2)))
    else:
        raise ValueError(f"{course_id} Unknown course id pattern")


def processTimeString(time: str, separator_range: str = "-", separator_time: str = ","):
    time_ranges = time.split(separator_time)
    converted_times = []
    for range in time_ranges:
        start_time, end_time = range.split(separator_range)
        converted_times.append((getDatetimeFromString(
            start_time), getDatetimeFromString(end_time)))
    return converted_times


def getDatetimeFromString(date: str):
    converted_date = None
    try:
        converted_date = datetime.strptime(date.strip(), DEFAULT_TIME_FORMAT)
    except ValueError:
        print(date)
        raise ValueError(f"date isn't in {DEFAULT_TIME_FORMAT} format")
    return converted_date


def getSlotCount(slot: tuple):
    if len(slot) != 2:
        raise ValueError("slot must only have a start and an end value")
    return slot[1] - slot[0]


def getDefaultSlotCount():
    return getSlotCount(getSlotInterval(CLASS_START_TIME, CLASS_END_TIME))


def getSlotInterval(starttime: datetime, endtime: datetime):
    if endtime < starttime:
        raise ValueError("End time must be greater than start time")
    if starttime < CLASS_START_TIME:
        raise ValueError("Any start time can't be lower than class start time")
    if endtime > CLASS_END_TIME:
        raise ValueError("Any end time must be bigger than class end time")
    start_slot = (starttime - CLASS_START_TIME).seconds/TIME_UNIT_S
    end_slot = (endtime - CLASS_START_TIME).seconds/TIME_UNIT_S
    # fractional slots aren't allowed
    return (int(start_slot), int(end_slot))


class WeekDays(Enum):
    SUNDAY = 0
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
